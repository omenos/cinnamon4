Name:		cinnamon-desktop
Version:	4.6.4
Release:	1%{?dist}
Summary:	Shared code among the Cinnamon environment

License:	GPLv2+ and LGPLv2+ and MIT
URL:		https://github.com/linuxmint/%{name}
Source0:	%{url}/archive/%{version}.tar.gz
Source1:	x-cinnamon-mimeapps.list

Patch0:		set_screensaver_font_default.patch

BuildRequires:	gcc
BuildRequires:	meson
BuildRequires:	ninja-build
BuildRequires:	intltool
BuildRequires:	python3
BuildRequires:	pkgconfig(gtk+-3.0)
BuildRequires:	pkgconfig(gdk-pixbuf-2.0)
BuildRequires:	pkgconfig(gio-2.0)
BuildRequires:	pkgconfig(glib-2.0)
BuildRequires:	pkgconfig(x11)
BuildRequires:	pkgconfig(xext)
BuildRequires:	pkgconfig(xkeyboard-config)
BuildRequires:	pkgconfig(xkbfile)
BuildRequires:	pkgconfig(xrandr)
BuildRequires:	pkgconfig(gobject-2.0)
BuildRequires:	pkgconfig(libpulse)
BuildRequires:	pkgconfig(libpulse-mainloop-glib)
BuildRequires:	pkgconfig(gobject-introspection-1.0)
BuildRequires:	pkgconfig(accountsservice)
BuildRequires:	pkgconfig(libstartup-notification-1.0)
Requires:		python3

%description
The cinnamon-desktop package contains an internal library
(libcinnamon-desktop) used to implement some portions of the CINNAMON
desktop, and also some data files and other shared components of the
CINNAMON user environment.

%package devel
Summary:	Libraries and headers for libcinnamon-desktop
License:	LGPLv2
Requires:	%{name}%{?_isa} = %{version}-%{release}

Requires:	gtk3-devel
Requires:	glib2-devel
Requires:	startup-notification-devel

%description devel
Libraries and header files for Cinnamon's libcinnamon-desktop development.

%prep
%autosetup -p1

%build
%meson -Dpnp_ids=/usr/share/hwdata/pnp.ids -Ddeprecation_warnings=false
%meson_build

%install
%meson_install
%find_lang cinnamon-desktop

mkdir -p %{buildroot}%{_datadir}/applications
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/x-cinnamon-mimeapps.list

%ldconfig_scriptlets

%files -f cinnamon-desktop.lang
%doc AUTHORS README MAINTAINERS
%license COPYING COPYING.LIB
%{_datadir}/applications/x-cinnamon-mimeapps.list
%{_datadir}/glib-2.0/schemas
%{_libdir}/*.so.*
%{_libdir}/girepository-1.0/C*.typelib

%files devel
%{_datadir}/gir-1.0/C*.gir
%{_includedir}/cinnamon-desktop/*
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.so

%changelog
* Wed Aug 26 2020 Mike Rochefort <mike@michaelrochefort.com> - 4.6.4-1
- Initial Build
