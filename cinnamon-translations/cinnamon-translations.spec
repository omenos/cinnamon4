Name:		cinnamon-translations
Version:	4.6.2
Release:	1%{?dist}
Summary:	Translations for Cinnamon and Nemo

License:	GPLv2
URL:		https://github.com/linuxmint/%{name}
Source0:	%{url}/archive/%{version}.tar.gz

BuildRequires:	coreutils
BuildRequires:	sed
BuildRequires:	gettext

BuildArch:		noarch

%description
Contains the translations used in Cinnamon and Nemo

%prep 
%setup -q

%build
%make_build

%install
%{__cp} -pr .%{_prefix} %{buildroot}
%find_lang cinnamon
%find_lang cinnamon-control-center
%find_lang cinnamon-screensaver
%find_lang cinnamon-session
%find_lang cinnamon-settings-daemon
%find_lang nemo
%find_lang nemo-extensions

%files -f cinnamon.lang -f cinnamon-control-center.lang -f cinnamon-screensaver.lang -f cinnamon-session.lang -f cinnamon-settings-daemon.lang -f nemo.lang -f nemo-extensions.lang
%license COPYING

%changelog
* Wed Aug 26 2020 Mike Rochefort <mike@michaelrochefort.com> - 4.6.2-1
- Initial Build

