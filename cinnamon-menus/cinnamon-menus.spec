Name:		cinnamon-menus
Version:	4.6.1
Release:	1%{?dist}
Summary:	Editor, configuration files, and library for Cinnamon menus.

License:	GPLv2+ and LGPLv2+
URL:		https://github.com/linuxmint/%{name}
Source0:	%{url}/archive/%{version}.tar.gz

BuildRequires:	gcc
BuildRequires:	meson
BuildRequires:	python3
BuildRequires:	intltool
BuildRequires:	pkgconfig(gio-unix-2.0)
BuildRequires:	pkgconfig(gobject-introspection-1.0)

%description
cinnamon-menus contains the libcinnamon-menu library, the layout configuration
files for the Cinnamon menu, as well as a simple menu editor.

The libcinnamon-menu library implements the "Desktop Menu Specification"
from freedesktop.org

%package devel
Summary:	Libraires and header files for the Cinnamon menu system
Requires:	%{name}%{_?isa} = %{version}-%{release}

%description devel
This package provides the necessary development libraries for
writing applications that use the Cinnamon menu system.

%prep
%setup -q

%build
%meson -Ddeprecated_warnings=false -Denable_debug=false
%meson_build

%install
%meson_install

%files
%doc README AUTHORS
%license COPYING COPYING.LIB
%{_libdir}/lib*.so.*
%{_libdir}/girepository-1.0/C*.typelib

%files devel
%{_datadir}/gir-1.0/C*.gir
%{_includedir}/cinnamon-menus-3.0/*
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*.pc

%changelog
* Wed Aug 26 2020 Mike Rochefort <mike@michaelrochefort.com> - 4.6.1-1
- Initial Build
